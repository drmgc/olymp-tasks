#!/usr/bin/env python3
# c.5, 1) - Задачи на нахождение делителей числа
# (Олимпиадные задания с решениями. 9-11 классы - Э. С. Ларина)

from sys import exit

def main():
	try:
		num = int(input("(Число)>> "))
	except ValueError:
		print("ОШИБКА: Вводить можно только целое число")
		return False
	divs = []
	for n in range(1, num + 1):
		if not num % n:
			divs.append(n)
	print("Делители:", *divs)
	return True

if __name__ == "__main__":
	try:
		while True:
			if not main():
				main()
	except KeyboardInterrupt:
		pass
	