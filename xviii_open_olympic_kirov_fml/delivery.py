#!/usr/bin/env python3

# XVIII Открытая олимпиада "Командный турнир по программированию"
# Киров, ФИЛ, 25 окт 2015
# Задача D. Доставка футболок

import sys

if '-v' in sys.argv:
	verbose = print
else:
	verbose = lambda *a, **kv: None

def main():
	length, waiting_limit, transfer_time = [int(i) for i in input().split()]
	in_way_times = [int(i) for i in input().split()]
	receivers = [int(i) for i in input().split()]

	time = 0
	for i in range(0, length):
		crr_in_way_time = in_way_times[i]
		crr_receiver = receivers[i]
		verbose(i)
		verbose('\ttime:', time)
		verbose('\tcrr_in_way_time:', crr_in_way_time)
		verbose('\tcrr_receiver:', crr_receiver)

		time += crr_in_way_time
		go_away_time = time + waiting_limit
		verbose('\tgo_away_time:', go_away_time)
		if go_away_time >= crr_receiver:
			verbose('\tDELIVERED')
			time += transfer_time
		else:
			time = go_away_time
	print(time)

if __name__ == '__main__':
	main()
